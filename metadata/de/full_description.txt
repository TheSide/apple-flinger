Kurzweiliges rundenbasiertes Spiel um sich die Zeit zu vertreiben. Für einen oder zwei Spieler.
Herausfordernd und süchtig machend.

Merkmale:

* Hochauflösende Grafik für Telefon und Tablet
* Sommer oder Winterumgebung
* Realistische Physik
* Ruckelfreies Spielen (auch auf alten Geräten)
* detaillierte Animationen und ein Partikelsystem
* brandneues und total innovatives Spielkonzept
* Einzel- und Mehrspielermodus
* Übersetzt in (fast) alle europäischen Sprachen
* 100% Open Source (GPL3)

Mit einer Zwille verschießt du Äpfel um die gegnerische Basis zu zerstören. Aber Vorsicht, die Gegenseite schießt zurück. Das Spiel ist eine ausgewogene Mischung aus Puzzle, Strategie, Geduld und Gefecht.

Für einen oder zwei Spieler! Du spielst gegen den Computer oder reihum mit jemandem, der neben dir sitzt.

Demnächst noch mehr Features, mehr Level, mehr Spaß.

Altersfreigaben:

* USK: 0
* ESRB: E (ab 6 Jahren geeignet)
* ACB: G
* PEGI:3
* ClassInd: L
* IARC: 3
